

document.addEventListener("DOMContentLoaded", function(){

    // Get objects from DOM
    var inputText = document.getElementById("inputText");
    var outputText = document.getElementById("outputText");
    var copyButton = document.getElementById("buttonCopyClipboard");
    var selectLinefeed = document.getElementById("optLinefeed");
    var selectLineSize = document.getElementById("optLineSize");
    
    // Local definitions of functions

    function getChunkSize( ){
        return Number(selectLineSize.value);
    }

    function getLinefeedReplacement( ){
        return selectLinefeed.options[selectLinefeed.selectedIndex].value;
    }

    function chunkString(str, size) {
        var numChunks = Math.ceil(str.length / size)
        var chunks = new Array(numChunks)
      
        for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
          chunks[i] = str.substr(o, size)
        }
      
        return chunks
      }

    function updateOutput( ){
        var linefeedSub  = getLinefeedReplacement( );
        var preparedText = inputText.value;
        preparedText = preparedText.replace(/(?:\r\n|\r|\n)/g, linefeedSub);

        var chunkSize = getChunkSize( );
        var chunks = chunkString(preparedText, chunkSize);
        outputText.value = '';
        for (let i = 0; i < chunks.length; i++) {
            if (i == 0) {
                outputText.value += 'DATA(lv_string) = |' + chunks[i] + '|\n';
            } else if ( i == chunks.length - 1 ) {
                outputText.value += '& |' + chunks[i] + '|.';
            } else {
                outputText.value += '& |' + chunks[i] + '|\n';
            }
        };
    }

    // Add Event-Listener
    inputText.addEventListener("input", updateOutput);
    selectLineSize.addEventListener("input", updateOutput);
    selectLinefeed.addEventListener("change", updateOutput);

    copyButton.addEventListener("click", function(){
        outputText.select();
        outputText.setSelectionRange(0, 99999); /*For mobile devices*/
        document.execCommand("copy");
    });
});

